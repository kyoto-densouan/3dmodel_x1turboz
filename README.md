# README #

1/3スケールのSHARP X-1 turboZ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- X1turbo III 1986年11月
- X1turboZ 1986年12月
- X1turboZ II 1987年12月
- X1turboZ III 1988年12月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [懐かしのパソコン](https://greendeepforest.com/?p=881)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turboz/raw/224d1fd9cb14d5f9679f3eec3474b133404608b1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turboz/raw/224d1fd9cb14d5f9679f3eec3474b133404608b1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turboz/raw/224d1fd9cb14d5f9679f3eec3474b133404608b1/ExampleImage.jpg)
